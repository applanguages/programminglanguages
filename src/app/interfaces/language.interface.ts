export interface Language {
    name: string;
    company: string;
    bio: string;
    key$?: string
};
