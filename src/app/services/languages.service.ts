import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Language } from './../interfaces/language.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class LanguagesService {
    languagesUrl = 'https://languagesapp-c68f3.firebaseio.com/language.json';
    languageUrl = 'https://languagesapp-c68f3.firebaseio.com/language/';
    
    constructor(private http: HttpClient) { }

    newLanguage(language: Language) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json'
            })
        };

        return this.http.post<Language>(this.languagesUrl, language , httpOptions)
            .pipe(
                map( res => {
                    console.log(res.name);
                    return res;
                })
            );
    }

    updateLanguage(language: Language, key$: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json'
            })
        };

        return this.http.put<Language>(`${ this.languageUrl }${ key$ }.json`, language , httpOptions)
            .pipe(
                map(res => {
                    console.log(res.name);
                    return res;
                })
            );
    }

    getLanguage (key$: string) {
        console.log(`${ this.languageUrl }${ key$ }.json`);
        return this.http.get<Language>(`${ this.languageUrl }${ key$ }.json`);
    }

    getLanguages () {
        return this.http.get<Language>(this.languagesUrl);
    }

    deleteLanguage (key$: string) {
        const url = `${ this.languageUrl }/${ key$ }.json`;
        console.log(url);

        return this.http.delete<Language>(url);
    }
}
