import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Language } from './../../interfaces/language.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { LanguagesService } from './../../services/languages.service';

@Component({
    selector: 'app-language',
    templateUrl: './language.component.html',
    styles: []
})

export class LanguageComponent implements OnInit {
    language: Language = {
        name: '',
        company: 'Google',
        bio: ''
    };

    new = false;
    id: string;

    constructor(private _languagesService: LanguagesService,
        private router: Router,
        private route: ActivatedRoute) {
        this.route.params.subscribe (params => {
            this.id = params['id'];
            if (this.id !== 'new') {
                this._languagesService.getLanguage(this.id)
                    .subscribe(data => this.language = data);
            }
        });
    }

    ngOnInit() {}

    save() {
        console.log('Save action');
        console.log(this.language);
        if (this.id === 'new') {
            console.log('new');
            this._languagesService.newLanguage(this.language)
                .subscribe(data => {
                    this.router.navigate(['/language', data.name]);
                },
                error => console.error(error));
        } else {
            console.log(`Update ${ this.id }`);
            this._languagesService.updateLanguage(this.language, this.id)
                .subscribe(data => {
                    console.log(data);
            },
            error => console.error(error));
        }
    }

    addNew(form: NgForm) {
        this.router.navigate(['/language', 'new']);
        form.onReset();
    }
}
