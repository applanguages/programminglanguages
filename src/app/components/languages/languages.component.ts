import { Component, OnInit } from '@angular/core';
import { LanguagesService } from './../../services/languages.service';
import { Language } from './../../interfaces/language.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../widgets/modal/modal.component';

@Component({
    selector: 'app-language',
    templateUrl: './languages.component.html',
    styles: [
        ` .table-responsive {
            margin-top: 15px;
          }
        `
    ]
})

export class LanguagesComponent implements OnInit {
    languages: Language;
    loading = true;

    constructor(private _languagesService: LanguagesService,
        private modalService: NgbModal) { }

    ngOnInit() {
        this._languagesService.getLanguages().subscribe((data: Language) => {
            this.languages = data;
            console.log(this.languages);
            this.loading = false;
        });
    }

    delete(key$: string) {
        const selectHeroQuestion = `¿Estás seguro de querer borrar el lenguaje ${ this.languages[key$].name }?`;
        const modalRef = this.modalService.open(ModalComponent);
        modalRef.componentInstance.title = 'Eliminar lenguaje';
        modalRef.componentInstance.body = selectHeroQuestion;
        modalRef.componentInstance.key$ = key$;
        modalRef.componentInstance.languages = this.languages;
    }
}
