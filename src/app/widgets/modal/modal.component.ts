import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LanguagesService } from '../../services/languages.service';

@Component({
    selector: 'app-modal-content',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})

export class ModalComponent {
    @Input() title;
    @Input() body;
    @Input() key$;
    @Input() languages;

    constructor(public activeModal: NgbActiveModal,
            private _languagesService: LanguagesService) { }

    confirm() {
        console.log('OK', this.key$);
        this._languagesService.deleteLanguage(this.key$)
            .subscribe(response => {
                console.log(response);
                if (response) {
                    console.error(response);
                } else {
                    delete this.languages[this.key$];
                }
            });

        this.activeModal.dismiss();
    }
}
