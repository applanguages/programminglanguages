import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { LanguagesComponent } from './components/languages/languages.component';
import { LanguageComponent } from './components/languages/language.component';
import { LoadingComponent } from './widgets/loading/loading.component';

import { LanguagesService } from './services/languages.service';
import { KeysPipe } from './pipes/keys.pipe';
import { ModalComponent } from './widgets/modal/modal.component';

@NgModule({
    declarations: [
        AppComponent,
        LanguagesComponent,
        LanguageComponent,
        KeysPipe,
        LoadingComponent,
        ModalComponent,
    ],
    imports: [
        APP_ROUTING,
        BrowserModule,
        HttpModule,
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot(),
    ],
    entryComponents: [ModalComponent],
    providers: [LanguagesService],
    bootstrap: [AppComponent]
})

export class AppModule { }
