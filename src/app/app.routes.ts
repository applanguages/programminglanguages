import { RouterModule,  Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LanguagesComponent } from './components/languages/languages.component';
import { LanguageComponent } from './components/languages/language.component';

export const APP_ROUTES: Routes = [
    { path: 'languages', component: LanguagesComponent},
    { path: 'language/:id', component: LanguageComponent },
    { path: '**', pathMatch: 'full' , redirectTo: 'languages' },
];

export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES, { useHash: true});
